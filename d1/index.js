//arrays: Arrays are a kind of predefined JS Object
//Because arrays have a constructor, a new array object must first be created before it can be used

//once a new array object is created, javascript's predefined array methods can be used

let students = ["John","Joe", "Jane", "Jessie"]
let first = students[0]
console.log(first)

let last = students[students.length-1]
console.log(last)

// //MAth: is another kind of predefined JS object is a math object.
// //unlike arrays, the Math object has no constructor. All of its properties and methods can be used without creating a math object beforehand. because of this, the math object is said to be "static"
// console.log(Math.PI)
// /*
// //other predefined Math properties

// Math.E = returns Euler's number
// Math.Pi = pi
// Math.SQRT2 = returns the squareroot of 2 or any number
// Math.Sqrt1_2 returns sq root of 1/2
// Math.LN2 = returns the natural log of 2 or any number
// Math.LOG2E = returns the base 2 logarithm of E or any other base
// */

// console.log(Math.SQRT2)
// console.log(Math.SQRT1_2)

// //math methods:
// console.log(Math.round(3.14)) //round to nearest integer
// console.log(Math.ceil(3.14)) //round up to the nearest integer (higher)
// console.log(Math.floor(3.14)) //round down to the nearest integer 
// console.log(Math.trunc(3.14)) //returns only the integer (no decimals)
// console.log(Math.min(-3,-2,-1,0,1,2,3)) //finds the lowest value in the list
// console.log(Math.max(-3,-2,-1,0,1,2,3)) //finds the highest value in the list
// console.log(Math.random())

// //Get a random integer between 0 and 10
// console.log(Math.floor(Math.random()*11))

